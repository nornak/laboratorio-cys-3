
kp = 50;
kd = 50;
num = [25*kd 25*kp ];
dem = [300 1+25*kd 1+25*kp];
funcion = tf(num, dem)

step(funcion);
xlabel('Tiempo t');
ylabel('Respuesta al escalon');
title('Grafico de respuesta con control proporcional y derivativo. kp=50 y kd=50');
grid on;
